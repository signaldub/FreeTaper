**FreeTaper - demon nagrywania dźwięku**

FreeTaper to program do nagrywania dźwięku, stworzony z myślą o rejestracji ciągłej.

Funkcje:
* nagrywanie surowego strumienia próbek ze standardowego wejścia dowolnego procesu, albo nagrywanie dźwięku z karty dźwiękowej (PortAudio)
* prawidłowo zaimplementowany dithering
* dzielenie części zgodne z zegarem czasu rzeczywistego

# Budowanie

Zależności (paczki w Ubuntu):

    portaudio19-dev libboost-system-dev libboost-iostreams-dev libboost-filesystem-dev cmake

Aby zbudować wpisz:

    git submodule update --init --recursive
    make


# Uruchamianie

    ./freetaper CONFIG

Gdzie `CONFIG` to ścieżka do pliku konfiguracyjnego. Co powinno się w nim znaleźć, jest omówione poniżej.

Jeżeli zapisujesz do plików WAV, upewnij się że w aktualnym katalogu znajduje się pomocniczy program `freetaper-writewav`. W przeciwnym wypadku katalog roboczy nie ma znaczenia.


# Architektura

Przechwytywanie -> Konwersja -> Zapis

Każdy etap jest realizowany w oddzielnym wątku, z wyjątkiem przechwytywania z innego procesu, kiedy wątek przechwytywania i zapisu jest współdzielony.


# Konfiguracja

Plik konfiguracyjny zgodny jest ze składnią [Hjson](http://hjson.org), czyli nadzbioru JSONa bardziej czytelnego dla człowieka. Przedstawione tu wartości są wzięte z przykładowego pliku `example.conf`.

## Parametry globalne

### Częstotliwość próbkowania
```
sample_rate: 44100
```
Częstotliwość próbkowania. Jeżeli FreeTaper nie będzie w stanie otworzyć urządzenia audio z tą częstotliwością, nie rozpocznie pracy.

### Priorytety wątków
```
#priority: {
#       converter: 20
#       writer: 10
#}
```
Można ustawić priorytet wątków aby zapobiec gubieniu danych jeżeli system jest obciążony. Uwaga: nie da się ustawić priorytetu wątku audio, gdyż za zarządzanie wątkiem audio odpowiada PortAudio.

Przy przechwytywaniu z JACKa robi to dobrze (zgodnie z konfiguracją serwera JACK), ale z ALSA ustawia priorytet jedynie +1. Wtedy zwiększanie priorytetów innych wątków nie ma sensu, dlatego ta sekcja w przykładzie jest wykomentowana, i w takim wypadku zalecane jest zwiększenie priorytetu całego FreeTapera za pomocą `nice(1)`, `chrt(1)` lub serwisu systemd.

W przypadku korzystania z JACKa zalecane jest tu ustawienie wartości niższych co najmniej o 20 od ustawienia priorytetu ("realtime priority") w serwerze JACK.

* `converter` - priorytet konwertera
* `writer` - priorytet zapisu

### Buforowanie
```
#buffer_seconds: {
#        pre_converter: 3,
#        post_converter: 20
#}
```
Rozmiary buforów między wątkami, w sekundach. Ta sekcja jest w przykładzie wykomentowana ponieważ podane w niej wartości są domyślne.


## Formaty próbek
Formaty są używane w kilku miejscach pliku konfiguracyjnego. Możliwe wartości:

* zmiennoprzecinkowe - `F` oznacza "floating point" (zmiennoprzecinkowy), a następująca dalej liczba to rozmiar próbki
    * `F32` - w języku C `float`
    * `F64` - w języku C `double`

* całkowite - `S` oznacza signed (liczba ze znakiem), a następująca dalej liczba to liczba bitów na próbkę
    * `S16`
    * `S24`
    * `S32`

W przypadku liczb całkowitych, można na końcu oznaczenia dodać `LE` (little endian - najmniej znaczący bajt na początku) lub `BE` (big endian - najmniej znaczący bajt na końcu). Domyślnie używana jest kolejność bajtów naturalna dla architektury na której uruchomiony jest FreeTaper. W większości architektur (x86, x86_64, ARM) jest to little endian.

## Przechwytywanie

Możliwe jest przechwytywanie z urządzeń audio lub ze standardowego wyjścia procesu. Jedna instancja FreeTapera przechwytuje z jednego wejścia, ale może mieć ono dowolną liczbę kanałów.

### Urządzenia audio
Przechwytywanie wejść urządzeń audio jest zrealizowane przez bibliotekę [PortAudio](http://www.portaudio.com/), więc na Linuksie obsługuje systemy dźwięku: ALSA, JACK i OSS (a także PulseAudio pośrednio przez ALSA plugin). Odpowiada za nie sekcja `capture` w pliku konfiguracyjnym:

```
capture: {
    type: audio
    engine: JACK .*
    device: Pulse.*
    input_buffer: 8192
    channels: 2
    format: F32
}
```

* `type: audio` - oznacza że przechwytujemy z urządzenia audio
* `engine` - wyrażenie regularne do którego zostanie dopasowana nazwa API używanego do przechwytywania dźwięku
* `device` - wyrażenie regularne do którego zostanie dopasowana nazwa urządzenia
* `input_buffer` - liczba próbek w cyklu
* `channels` - liczba kanałów
* `format` - format próbki

Aby wylistować urządzenia audio, uruchom FreeTapera wpisując nieistniejące API, np. `engine: notexisting`. Lista par `engine / device` pojawi się na konsoli.

### Proces

```
capture: {
    type: process
    args: [ '/bin/cat', '/dev/zero' ]
    channels: 2
    format: S32
}
```

* `type: process` - oznacza że przechwytujemy ze standardowego wyjścia procesu
* `args` - lista argumentów. Pierwszy argument musi być pełną ścieżką do pliku wykonywalnego
* `channels` - liczba kanałów. Wejście musi być przeplatane, tzn. próbki odpowiadające kolejnym kanałom muszą następować po sobie.
* `format` - format próbki

## Konwersja
Jeżeli formaty wejścia i wyjścia są różne, FreeTaper automatycznie konwertuje próbki:

* jeżeli próbka wyjściowa ma większą rozdzielczość niż wejściowa, brakujące bity wypełniane są zerami. Kodeki bezstratne, np. FLAC potrafią potem te zera efektywnie skompresować.
* jeżeli próbka wyjściowa ma mniejszą rozdzielczość niż wejściowa, próbki są ditherowane szumem o trójkątnym rozkładzie prawdopodobieństwa, co oznacza że **nie są wprowadzane zniekształcenia wynikające z obcinania bitów, lecz szum**.
Liczby zmiennoprzecinkowe (F32, F64) są zawsze traktowane jako posiadające większą rozdzielczość niż liczby całkowite.

Konwersja częstotliwości próbkowania nie jest możliwa w samym FreeTaperze. Jeżeli jest potrzebna, polecamy [SoX](http://sox.sourceforge.net/SoX/Resampling).

Konwerter nie ma własnej sekcji w pliku konfiguracyjnym. Format wyjściowy wybiera się w sekcji `output`.

## Zapis
Zapis posiada konfigurację globalną:

```
local_time: true
split: 3600
out_prefix: /storage/szpieg/%Y-%m-%d_%H-%M-%S_%Z.
```

* `local_time` - czy nazwy plików wyjściowych będą generowane na podstawie daty i godziny w lokalnej strefie czasowej? Jeżeli nie, używany jest czas uniwersalny (UTC).
    * Jeżeli wpiszesz tutaj `true`, rotacja plików wyjściowych (`split`) jest włączona i żyjesz w rejonie w którym obowiązuje zmiana czasu, upewnij się że w nazwach plików (`out_prefix`, `file_name`) znajdzie się strefa czasowa. Inaczej podczas zmiany czasu pliki mogą zostać nadpisane.
* `split` - co ile sekund (zegara czasu rzeczywistego) zaczynać nowy plik. `null` lub niewpisanie tego parametru wyłącza rotowanie plików.
    * Dokładniej, plik wyjściowy jest rotowany jeżeli wynik dzielenia całkowitego aktualnego *Unix timestamp* przez wartość `split` zmieni się. Oznacza to że gdy wpiszemy tu `3600`, pliki będą się zaczynać o pełnych godzinach, a `60` - o pełnych minutach.
* `out_prefix` - prefiks dodawany do każdego parametru `file_name` w sekcji `output`

Poszczególne wyjścia są konfigurowane w sekcji `outputs`:

```
outputs: [
    {
        channels: [1, 2]
        format: S16
        outputs: [
            { type: "FLAC", file_name: "flac" },
            { type: "Vorbis", file_name: "ogg" },
            { type: "WAV", file_name: "wav" }
        ]
    }
    {
        channels: [1, 2]
        format: S24
        type: FLAC
        file_name: 24bit.flac
    }
]
```

Każdy blok objęty klamrami w sekcji `outputs` to jedno wyjście, do którego formatu jest konwertowane wejście. Jego parametry:

* `channels` - lista kanałów branych z wejścia (numeracja od 1)
* `format` - format próbki
* `outputs` lub *parametry pojedynczego pliku wyjściowego*:
    * jeżeli jest podany `outputs`, jest on listą plików wyjściowych do których zostaną zapisane te same dane audio (tak jak pierwsze wyjście w przykładzie)
    * w przeciwnym wypadku należy podać *parametry pojedynczego pliku wyjściowego* na tym samym poziomie co resztę parametrów (tak jak drugie wyjście w przykładzie)

### Parametry pojedynczego pliku wyjściowego

* `type` - format pliku wyjściowego, możliwe wartości to: `FLAC`, `Vorbis`, `WAV`.
* `file_name` - ścieżka i nazwa pliku wyjściowego
    * jeżeli istnieje parametr globalny `out_prefix`, jest doklejany na początku
    * tuż przed utworzeniem pliku wyjściowego, podany tutaj tekst jest przepuszczany przez funkcję `strftime(3)`. Można więc w nim stosować ciągi formatujące zaczynające się od znaku `%`, wstawiające datę i czas.
    * jeżeli nie istnieje katalog docelowy to zostanie utworzony przed utworzeniem pliku. Oznacza to że można zawrzeć zmienne daty i czasu w ścieżce, np. `/storage/szpieg/%Y-%m/%d/%Y-%m-%d_%H-%M-%S_%Z.flac`

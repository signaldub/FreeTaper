**FreeTaper - audio recorder daemon**

FreeTaper is an audio recorder created for continuous recording.

Features:
* captures raw samples from any process' stdout, or from soundcard
* dithers properly
* splits parts according to real-time-clock

# Building

Dependencies (Ubuntu package names):

    portaudio19-dev libboost-system-dev libboost-iostreams-dev libboost-filesystem-dev cmake

To build:

    git submodule update --init --recursive
    make


# Running

    ./freetaper CONFIG_PATH

where `CONFIG_PATH` is path to configuration file. See below for its details.

If you're writing to WAV, ensure that your working directory contains helper executable `freetaper-writewav`. Otherwise working directory doesn't matter.


# Architecture

Capture -> Conversion -> Writing

Every stage is done in separate thread, except process capture which shares thread with writer.


# Configuration

Configuration syntax follows [Hjson](http://hjson.org) syntax. Values shown in this manual are taken from `example.conf` file.

## Global parameters

### Sample rate
```
sample_rate: 44100
```
Sample rate. If FreeTaper isn't able to open audio device with that frequency, it won't record and will exit immediately.

### Thread priorities
```
#priority: {
#       converter: 20
#       writer: 10
#}
```
You can set thread priorities to prevent dropouts when system is loaded. Note: you can't set audio thread priority, because it's managed by PortAudio library.

With JACK it is set correctly (in accordance with JACK daemon settings) but with ALSA only +1 priority is set, so increasing priorities of other threads doesn't make sense. That's why this section is commented out in the example. When capturing from ALSA it's best to increase priority of the whole FreeTaper process using `nice(1)`, `chrt(1)` or systemd service configuration.

When capturing from JACK, it's advised to set these values to at most `JACK_priority - 20`, where `JACK_priority` is realtime priority setting of the JACK server.

* `converter` - conversion priority
* `writer` - writing priority


### Buffering
```
#buffer_seconds: {
#        pre_converter: 3,
#        post_converter: 20
#}
```
Inter-thread buffer sizes, in seconds. This section is commented out in the example because those values are default.


## Sample formats
Sample formats are used in several places of the configuration file. Possible values:

* floating point - `F`
    * `F32` - `float` in C
    * `F64` - `double` in C
* signed integer - `S`
    * `S16`
    * `S24`
    * `S32`

You can append `LE` (little endian) or `BE` (big endian) to the designation of an integer format. By default CPU-native endianness is used. For most architectures (x86, x86_64, ARM) it is little endian.

## Capture

It's possible to capture from audio devices or standard output of another process. A single instance of FreeTaper can capture from a single input, but it can have arbitrary number of channels.

### Audio devices
Capture from audio devices is done by [PortAudio](http://www.portaudio.com/) library. It supports ALSA, JACK and OSS (and PulseAudio indirectly via ALSA plugin) on GNU/Linux systems. (FreeTaper should - in theory - support other operating systems but wasn't tested on them yet.) It is configured in `capture` section in configuration file:

```
capture: {
    type: audio
    engine: JACK .*
    device: Pulse.*
    input_buffer: 8192
    channels: 2
    format: F32
}
```

* `type: audio` - means that we're capturing from an audio device
* `engine` - regular expression to match the PortAudio backend API
* `device` - regular expression to match the device name
* `input_buffer` - samples per capture cycle
* `channels` - channels count
* `format` - sample format

To list audio devices, launch FreeTaper with non-existing API, e.g.. `engine: notexisting`. `Engine / device` pairs will be printed on the console.

### Process

```
capture: {
    type: process
    args: [ '/bin/cat', '/dev/zero' ]
    channels: 2
    format: S32
}
```

* `type: process` - means that we're capturing from a standard output of another process
* `args` - arguments list. First argument must be a full path to the executable.
* `channels` - number of channels. Channels are expected to be interleaved.
* `format` - sample format

## Conversion
If input and output sample formats differ, FreeTaper converts samples automatically:

* if the output sample has higher resolution than the input sample, missing bits are filled with zeroes.
* it the output sample has lower resolution than the input sample, samples are dithered using TPDF (triangular probability density function) noise which means that **no distortion is introduced, only some noise**.
Floating point numbers (F32, F64) are always treated as having more resolution than integer numbers.

Sample rate conversion isn't possible within FreeTaper itself. We recommend [SoX](http://sox.sourceforge.net/SoX/Resampling) for doing it.

Converter doesn't have its own section in configuration file. Output format is to be specified in the `output` section.

## Writing
Writing has a global configuration:

```
local_time: true
split: 3600
out_prefix: /storage/spy/%Y-%m-%d_%H-%M-%S_%Z.
```

* `local_time` - `true` to use local time in file names, `false` to use UTC
    * If you specify `true` here, segmenting (`split`) is enabled, and you live in region with daylight saving time (DST), make sure that output file names (`out_prefix`, `file_name`) will have timezone in them. Otherwise files will be overwritten.
* `split` - interval (in wall clock seconds) between start of each segment. `null` or not setting this parameter disables segmenting.
    * To be exact: output file is rotated if the value of current Unix timestamp, divided by `split`, rounded down to integer, changes. It means that, for example, if you specify `3600` here, then no matter what time you start recording, the parts will start at the top of hour.
* `out_prefix` - prefix prepended to every `file_name` parameter in `output` section

Individual outputs are configured in `outputs` section:

```
outputs: [
    {
        channels: [1, 2]
        format: S16
        outputs: [
            { type: "FLAC", file_name: "flac" },
            { type: "Vorbis", file_name: "ogg" },
            { type: "WAV", file_name: "wav" }
        ]
    }
    {
        channels: [1, 2]
        format: S24
        type: FLAC
        file_name: 24bit.flac
    }
]
```

Each block embraced with curly braces within the `outputs` section is an individual output. Input is converted to the output format automatically. Output parameters:

* `channels` - list of channels taken from input (first is 1)
* `format` - sample format
* `outputs` or *single output file parameters*:
    * if `outputs` is given, it must be a list of output files to which identical audio data will be written. First output in the example depicts this.
    * otherwise, *single output file parameters* should be given on the same level as other parameters. Second output in the example depicts this.

### Single output file parameters

* `type` - output file format, possible values are: `FLAC`, `Vorbis`, `WAV`.
* `file_name` - path and name of the output file
    * if a global parameter `out_prefix` exists, it is prepended
    * just before a new file is created, the string specified here is passed through `strftime(3)` function. So you can used date & time formatting strings in it.
    * if output directory doesn't exist, it will be created before writing file. It means that you can have date-and-time-dependent directory names in path, e.g. `/storage/spy/%Y-%m/%d/%Y-%m-%d_%H-%M-%S_%Z.flac`

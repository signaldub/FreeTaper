#include "util.hpp"
#include <sched.h>
#include <sys/prctl.h>
#include <limits.h>

void set_thread_name(const char* name) {
    prctl(PR_SET_NAME, (unsigned long)name, 0, 0, 0);
}
void set_thread_name(const std::string &name) {
    set_thread_name(name.c_str());
}

bool setThreadRealTime(std::thread &thr, int prio) {
    if (prio==INT_MIN) return true;
    sched_param sp{0};
    sp.sched_priority = prio;
    int ret = pthread_setschedparam(thr.native_handle(), SCHED_FIFO, &sp);
    if (ret==0) {
        return true;
    } else {
        perror("setThreadRealTime: pthread_setschedparam failed");
        return false;
    }
}


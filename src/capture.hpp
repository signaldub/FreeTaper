#pragma once
#include <memory>
#include "util.hpp"
#include "specs.hpp"
#include "RingBuffer.hpp"
#include "hjson_sugar.hpp"

class ICapture {
public:
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual unsigned overflowCount() const = 0;
    virtual size_t totalFrames() const = 0;
    virtual int sampleRate() const { return -1; };
    virtual void setRingBuffer(RingBuffer<uint8_t>* buffer) = 0;
    virtual ~ICapture() {
    }
};

class CaptureFactory {
public:
    using CapturePtr = std::unique_ptr<ICapture>;
    static CapturePtr create(JsonValue config);
    static PipeSpec getSpec(JsonValue config);
};

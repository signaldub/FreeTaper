#include <boost/asio.hpp>

int global_prio_converter = INT_MIN;
int global_prio_writer = INT_MIN;

int global_sample_rate = 0; // FIXME: handle sample rate autodetection

bool global_use_local_time = false;

boost::asio::io_service global_io_service;

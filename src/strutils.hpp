#pragma once
#include <string>

namespace strutils {
    inline bool isWhiteSpace(const char c) {
        return (c==' ') || (c=='\t') || (c=='\r') || (c=='\n');
    }
    std::string trim(const std::string &s);
    void toLowerInPlace(std::string &s);
};

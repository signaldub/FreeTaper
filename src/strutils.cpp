#include "strutils.hpp"

namespace strutils {
    std::string trim(const std::string &s) {
        size_t startfrom = s.length();
        for (size_t i=0; i<s.length(); i++) {
            if (!isWhiteSpace(s[i])) {
                startfrom = i;
                break;
            }
        }
        size_t endat = 0;
        size_t i = s.length();
        do {
            i--;
            if (!isWhiteSpace(s[i])) {
                endat = i;
                break;
            }
        } while (i>0);
        if (startfrom <= endat) {
            return s.substr(startfrom, endat-startfrom+1);
        } else {
            return "";
        }
    }
    void toLowerInPlace(std::string &s) {
        for (char &c: s) {
            if (c >= 'A' && c <= 'Z') {
                c += 'a'-'A';
            }
        }
    }
};

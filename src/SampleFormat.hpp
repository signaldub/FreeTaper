#pragma once
#include <string>
#include <stdexcept>
#include "strutils.hpp"

#if ENABLE_PORTAUDIO_CAPTURE
#include <portaudio.h>
#endif


class SampleFormat {
public:
    enum NumberType {
        NONE,
        S16,
        S24,
        S32,
        LOWEST_FLOAT,
        FLOAT32 = LOWEST_FLOAT,
        FLOAT64,
        NB
    };
    enum {
        NUMBER_TYPE_MASK = 0x00ff,
        ENDIANNESS_MASK = 0x0f00
    };
    enum Endianness {
        ENDIAN_NATIVE = 0x0000,
        ENDIAN_LITTLE = 0x0100,
        ENDIAN_BIG = 0x0200
    };
protected:
    uint_fast16_t value_;
    static constexpr uint_fast8_t sizes_bytes[NB] = { 0, 2, 3, 4, 4, 8 };
#if ENABLE_PORTAUDIO_CAPTURE
    static constexpr PaSampleFormat pa_mappings[NB] = { 0, paInt16, paInt24, paInt32, paFloat32, 0 };
#endif
    #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        static constexpr Endianness native_endianness = ENDIAN_LITTLE;
    #elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        static constexpr Endianness native_endianness = ENDIAN_BIG;
    #else
        #error unsupported endianness
    #endif
public:
    operator int() const {
        return value_;
    }
    NumberType numberType() const {
        return (NumberType)(value_ & NUMBER_TYPE_MASK);
    }
    Endianness endianness() const {
        return (Endianness)(value_ & ENDIANNESS_MASK);
    }
    Endianness realEndianness() const {
        return endianness()!=ENDIAN_NATIVE ? endianness() : native_endianness;
    }
    operator bool() const {
        return value_ != NONE;
    }
    SampleFormat(const uint_fast16_t value): value_(value) {
    }
    SampleFormat(): value_(NONE) {
    }
    uint_fast8_t bytes() const {
        return sizes_bytes[numberType()];
    }
#if ENABLE_PORTAUDIO_CAPTURE
    PaSampleFormat paSampleFormat() const {
        if (pa_mappings[numberType()]==0) throw std::runtime_error("Sample format not compatible with PortAudio");
        return pa_mappings[numberType()];
    }
#endif
    uint_fast16_t bits() const {
        return bytes() * 8;
    }
    bool isLinear() const {
        return numberType() < LOWEST_FLOAT; // floating point numbers don't have linear scale (precision depends on value)
    }
    bool isFloat() const {
        return numberType() >= LOWEST_FLOAT;
    }
    #define CMP_OPERATOR(oper) bool operator oper (const SampleFormat other) const { \
        if (isFloat() == other.isFloat()) { \
            return (uint_fast16_t)(numberType()) oper (uint_fast16_t)(other.numberType()); \
        } else { \
            return (isFloat() ? 1 : 0) oper (other.isFloat() ? 1 : 0); \
        } \
    }
    CMP_OPERATOR(>);
    CMP_OPERATOR(<);
    CMP_OPERATOR(>=);
    CMP_OPERATOR(<=);
    #undef CMP_OPERATOR
    bool operator==(const SampleFormat other) const {
        return value_ == other.value_;
    }
    bool operator!=(const SampleFormat other) const {
        return value_ != other.value_;
    }
    static SampleFormat intFromBitsCount(uint_fast16_t bits_count, Endianness endianness = ENDIAN_NATIVE) {
        if (bits_count<=16) return S16 | endianness;
        if (bits_count<=24) return S24 | endianness;
        if (bits_count<=32) return S32 | endianness;
        return NONE;
    }
    static SampleFormat fromName(std::string name);
};

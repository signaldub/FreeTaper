#pragma once
#include "util.hpp"
#include "SampleFormat.hpp"

class SampleConverterImpl;

using ConversionFunction = void(void*, const uint8_t* src_ptr, const size_t src_stride, uint8_t* dst_ptr, const size_t dst_stride, size_t &position, const size_t count);

class SampleConverter {
protected:
    std::unique_ptr<SampleConverterImpl> impl_;
public:
    SampleConverter();
    ~SampleConverter();
    void* getFirstArgument() {
        return impl_.get();
    }
    ConversionFunction* getConversionFunction(const SampleFormat in, const SampleFormat out);
};

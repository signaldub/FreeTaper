#pragma once
#include "SampleFormat.hpp"
#include <vector>

using ChannelNumber = uint_fast16_t;

struct StreamHandlerSpec {
    std::string name;
    std::string arg;
};

struct StreamSpec {
    SampleFormat format;
    std::vector<ChannelNumber> channels;
    std::vector<StreamHandlerSpec> handlers;
    unsigned int bytesPerFrame() {
        return channels.size() * format.bytes();
    }
};

struct PipeSpec {
    std::vector<StreamSpec> streams;
};

#pragma once

#include <vector>
#include <string>

struct ProcessExecArgs {
    std::string exe_path;
    std::vector<std::string> args;
    template<typename ...Ts> ProcessExecArgs(const std::string path, Ts ... a_args):
        exe_path(path),
        args(std::initializer_list<std::string>{a_args...}) {
    }
    template<typename Container> static ProcessExecArgs fromList(Container arr) {
        ProcessExecArgs pea;
        pea.exe_path = (const char*)arr[0];
        pea.args.reserve(arr.size()-1);
        for (int i=1; i<arr.size(); i++) {
            pea.args.push_back((const char*)arr[i]);
        }
        return pea;
    }
private:
    ProcessExecArgs() {}
};

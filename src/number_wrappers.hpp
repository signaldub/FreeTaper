#pragma once
#include <cstdint>

//#define IS_BIG_ENDIAN (*(uint16_t *)"\0\xff" < 0x100)

template<typename NativeT, size_t storage_bytes = sizeof(NativeT)> class IntWrapper {
    static_assert(sizeof(NativeT) >= storage_bytes, "storage size can't be bigger than native size");
protected:
    static constexpr NativeT bitmask = static_cast<NativeT>((1LL << (storage_bytes*8)) - 1LL);
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    static constexpr bool IS_CPU_BIG_ENDIAN = false;
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    static constexpr bool IS_CPU_BIG_ENDIAN = true;
#else
    #error unsupported endianness
#endif
    NativeT value_;
public:
    using ValueType = NativeT;
    operator NativeT() const __attribute__((always_inline)) {
        return value_;
    }
    IntWrapper& operator = (const IntWrapper &other) __attribute__((always_inline)) {
        value_ = other.value_;
        return *this;
    }
    IntWrapper& operator = (const NativeT val) __attribute__((always_inline)) {
        value_ = val & bitmask;
        return *this;
    }
    NativeT get() const __attribute__((always_inline)) {
        return value_;
    }
    IntWrapper(const NativeT val = 0) __attribute__((always_inline)): value_(val & bitmask) {
    }
    IntWrapper(const IntWrapper &other) __attribute__((always_inline)): value_(other.value_) {
    }
    static constexpr size_t size = storage_bytes;
    static constexpr bool is_signed = (NativeT)(-1) < 0;
    static constexpr NativeT max_value = is_signed ? static_cast<NativeT>((1LL << (storage_bytes*8 - 1)) - 1LL) : bitmask;
    static constexpr NativeT min_value = is_signed ? static_cast<NativeT>(-(1LL << (storage_bytes*8 - 1))) : 0;
    static constexpr bool is_int = true;
    static constexpr bool is_float = false;
    void writeTo(void* ptr, const bool big_endian = IS_CPU_BIG_ENDIAN) const __attribute__((always_inline)) {
        if ((storage_bytes != sizeof(NativeT)) || (big_endian != IS_CPU_BIG_ENDIAN)) {
            // write each byte manually
            NativeT v = value_;
            if (!big_endian) {
                for (int i=0; i<storage_bytes; i++) {
                    *(static_cast<uint8_t*>(ptr) + i) = v;
                    v >>= 8;
                }
            } else {
                for (int i=0; i<storage_bytes; i++) {
                    *(static_cast<uint8_t*>(ptr) + i) = v >> ((storage_bytes-1)*8 - i*8);
                }
            }
        } else {
            // use native memory write
            *static_cast<NativeT*>(ptr) = value_;
        }
    }
    void readFrom(const void* ptr, const bool big_endian = IS_CPU_BIG_ENDIAN) __attribute__((always_inline)) {
        if ((storage_bytes != sizeof(NativeT)) || (big_endian != IS_CPU_BIG_ENDIAN)) {
            // read each byte manually
            value_ = 0;
            if (!big_endian) {
                for (int i=0; i<storage_bytes; i++) {
                    value_ |= *(static_cast<const uint8_t*>(ptr) + i) << (i*8);
                }
            } else {
                for (int i=0; i<storage_bytes; i++) {
                    value_ <<= 8;
                    value_ |= *(static_cast<const uint8_t*>(ptr) + i);
                }
            }
        } else {
            // use native memory read
            value_ = *static_cast<const NativeT*>(ptr);
        }
    }
    IntWrapper(const void* ptr, const bool big_endian = IS_CPU_BIG_ENDIAN) __attribute__((always_inline)) {
        readFrom(ptr, big_endian);
    }
};

template<typename T> class FloatWrapper {
    T value_;
public:
    using ValueType = T;
    operator T() const __attribute__((always_inline)) {
        return value_;
    }
    T get() const __attribute__((always_inline)) {
        return value_;
    }
    FloatWrapper& operator = (const FloatWrapper &other) __attribute__((always_inline)) {
        value_ = other.value_;
        return *this;
    }
    FloatWrapper& operator = (const T val) __attribute__((always_inline)) {
        value_ = val;
        return *this;
    }
    FloatWrapper(const T val = 0) __attribute__((always_inline)): value_(val) {
    }
    FloatWrapper(const FloatWrapper &other) __attribute__((always_inline)): value_(other.value_) {
    }
    static constexpr size_t size = sizeof(T);
    static constexpr bool is_signed = true;
    static constexpr T max_value = 1;
    static constexpr T min_value = -1;
    static constexpr bool is_int = false;
    static constexpr bool is_float = true;
    // FIXME: don't ignore big_endian argument
    void writeTo(void* ptr, const bool) const __attribute__((always_inline)) {
        *static_cast<T*>(ptr) = value_;
    }
    void readFrom(const void* ptr, const bool) __attribute__((always_inline)) {
        value_ = *static_cast<const T*>(ptr);
    }
    FloatWrapper(const void* ptr, const bool big_endian) __attribute__((always_inline)) {
        readFrom(ptr, big_endian);
    }
};

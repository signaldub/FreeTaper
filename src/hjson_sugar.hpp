#pragma once

#include "hjson.h"
#include <functional>

namespace Hjson {

static void forEachInVector(Hjson::Value vec, std::function<void(Hjson::Value&)> cb) {
    for (size_t i=0; i<vec.size(); i++) {
        cb(vec[i]);
    }
}

};

using JsonValue = Hjson::Value;

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// usage:
//  ./freetaper-writewav out.wav 44100 16   2
//                       file    rate  bits channels

#define HEADER_SIZE 44

int main(int argc, char** argv) {
    if (argc<=4) {
        fprintf(stderr, "Usage: %s OUTPUT_FILE SAMPLE_RATE BITS_PER_SAMPLE CHANNELS\n", argv[0]);
        return 1;
    }
    
    const size_t buffer_size = 65536;
    uint8_t* buffer = malloc(buffer_size);
    if (!buffer) {
        perror("malloc");
        return 1;
    }
    
    FILE* out = fopen(argv[1], "w");
    if (!out) {
        perror("output open");
        return 1;
    }
    
    uint16_t channels_count = atol(argv[4]);
    uint32_t sample_rate = atoi(argv[2]);
    uint16_t bytes_per_sample = atoi(argv[3]) / 8;
    uint16_t bits_per_sample = bytes_per_sample * 8;
    uint16_t bytes_per_frame = channels_count * bytes_per_sample;
    uint32_t byte_rate = sample_rate * bytes_per_frame;
    #define le16(x) (x), (x) >> 8
    #define le32(x) (x), (x) >> 8, (x) >> 16, (x) >> 24
    uint8_t header[HEADER_SIZE] = { 'R', 'I', 'F', 'F', /* file_size - 8: */ 0, 0, 0, 0, 'W', 'A', 'V', 'E',
                           'f', 'm', 't', ' ', /* this chunk length: */ 16, 0, 0, 0,
                           /* audio format: */ 1, 0, /* = PCM */
                           /* channels count: */ le16(channels_count),
                           /* sample rate: */ le32(sample_rate),
                           /* byte rate: */ le32(byte_rate),
                           /* bytes per frame: */ le16(bytes_per_frame),
                           /* bits per sample: */ le16(bits_per_sample),
                           'd', 'a', 't', 'a', /* data size: */ 0, 0, 0, 0 };
    
    if (fwrite(header, 1, HEADER_SIZE, out) != HEADER_SIZE) {
        perror("initial header write");
        return 1;
    }
    
    size_t total_bytes = 0;
    
    while (!feof(stdin)) {
        size_t bytes_read = fread(buffer, 1, buffer_size, stdin);
        if (bytes_read==0) {
            break;
        }
        size_t bytes_written = fwrite(buffer, 1, bytes_read, out);
        total_bytes += bytes_written;
        if (bytes_read != bytes_written) {
            fprintf(stderr, "Write error. Disk full?\n");
            break;
        }
    }
    
    free(buffer);
    
    if (fseek(out, 0, SEEK_SET)) {
        perror("seek to header");
    } else {
        uint32_t s = total_bytes + 36;
        header[4] = s;
        header[5] = s >> 8;
        header[6] = s >> 16;
        header[7] = s >> 24;
        
        s = total_bytes;
        header[40] = s;
        header[41] = s >> 8;
        header[42] = s >> 16;
        header[43] = s >> 24;
    
        if (fwrite(header, 1, HEADER_SIZE, out) != HEADER_SIZE) {
            perror("final header write");
        }
    }
    fclose(out);
    
    return 0;
}

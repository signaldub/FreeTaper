#pragma once

#include <thread>
#include <string>

void set_thread_name(const char* name);
void set_thread_name(const std::string &name);
bool setThreadRealTime(std::thread &thr, int prio);


#if __cplusplus >= 201402L
using std::make_unique;
#else
template<typename T, typename ...Args> std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
#endif

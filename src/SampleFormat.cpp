#include "SampleFormat.hpp"


constexpr uint_fast8_t SampleFormat::sizes_bytes[NB];
#if ENABLE_PORTAUDIO_CAPTURE
constexpr PaSampleFormat SampleFormat::pa_mappings[NB];
#endif

SampleFormat SampleFormat::fromName(std::string name) {
    strutils::toLowerInPlace(name);
    std::string s1 = name.substr(0, 3);
    
    NumberType ntype = NONE;
    if (s1==std::string("s16")) ntype = S16; else
    if (s1==std::string("s24")) ntype = S24; else
    if (s1==std::string("s32")) ntype = S32; else
    if (s1==std::string("f32")) ntype = FLOAT32; else
    if (s1==std::string("f64")) ntype = FLOAT64;
    
    std::string s2 = name.substr(3);
    Endianness endian = ENDIAN_NATIVE;
    if (s2==std::string("le")) endian = ENDIAN_LITTLE; else
    if (s2==std::string("be")) endian = ENDIAN_BIG; else
    if (!s2.empty()) ntype = NONE;
    
    if (ntype==NONE) {
        throw std::runtime_error("Couldn't parse sample format name");
    }
    return ntype | endian;
}

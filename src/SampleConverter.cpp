#include "SampleConverter.hpp"
#include <random>
#include <algorithm>
#include "number_wrappers.hpp"

class SampleConverterImpl {
    static_assert(((-1) >> 1)==-1, "right bit shift doesn't extend sign, or architecture isn't two's complement");
protected:
    std::mt19937 random_engine_;
public:
    SampleConverterImpl() {
        std::random_device rd;
        std::array<int, std::mt19937::state_size> seed_data;
        std::generate_n(seed_data.data(), seed_data.size(), std::ref(rd));
        std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
        random_engine_ = std::mt19937(seq);
    }
    template<typename T> __attribute__((always_inline)) T getFloatDitherSample() {
        return ( static_cast<T>(random_engine_())/static_cast<T>(random_engine_.max()) -
        static_cast<T>(random_engine_())/static_cast<T>(random_engine_.max()) );
    }
    template<typename T, unsigned bits> __attribute__((always_inline)) T getIntDitherSample()  {
        static constexpr unsigned bitmask = (1L<<bits)-1;
        return (random_engine_() & bitmask) - (random_engine_() & bitmask);
    }
    
    template<typename InT, typename OutT> __attribute__((always_inline)) typename std::enable_if<InT::is_float && OutT::is_float>::type convertValue(const InT in, OutT &out) {
        out = in;
    }
    template<typename InT, typename OutT> __attribute__((always_inline)) typename std::enable_if<InT::is_float && OutT::is_int>::type convertValue(const InT in, OutT &out) {
        typename InT::ValueType flt = in.get() * static_cast<typename InT::ValueType>(OutT::max_value) + getFloatDitherSample<typename InT::ValueType>();
        flt += ((flt>=0) ? 0.5 : -0.5);
        if (flt > OutT::max_value) {
            out = OutT::max_value;
        } else if (flt < OutT::min_value) {
            out = OutT::min_value;
        } else {
            out = static_cast<typename OutT::ValueType>(flt);
        }
    }
    template<typename InT, typename OutT> __attribute__((always_inline)) typename std::enable_if<InT::is_int && OutT::is_float>::type convertValue(const InT in, OutT &out) {
        out = in.get() * static_cast<typename OutT::ValueType>(-1.0f / OutT::min_value);
    }
    template<typename InT, typename OutT> __attribute__((always_inline)) typename std::enable_if<InT::is_int && OutT::is_int && (InT::size>OutT::size)>::type convertValue(const InT in, OutT &out) {
        static constexpr unsigned bits_to_reduce = (InT::size-OutT::size)*8;
        static constexpr typename InT::ValueType half_step = 1L << (bits_to_reduce-1);
        typename InT::ValueType dither = getIntDitherSample<typename InT::ValueType, bits_to_reduce>();
        // this condition must be based on input, not output sample,
        // because otherwise dither might cause integer wrap (e.g. +32767 to -32768 - very unpleasant sound)
        // it has downside of not dithering peaks, but it doesn't matter if signal has proper headroom
        if (in >= InT::max_value) {
            out = OutT::max_value;
        } else if (in <= InT::min_value) {
            out = OutT::min_value;
        } else {
            out = static_cast<typename OutT::ValueType>((in.get() + dither + half_step) >> bits_to_reduce);
        }
    }
    template<typename InT, typename OutT> __attribute__((always_inline)) typename std::enable_if<InT::is_int && OutT::is_int && (InT::size<=OutT::size)>::type convertValue(const InT in, OutT &out) {
        out = in.get() << ((OutT::size-InT::size)*8);
    }
    template<typename InT, bool in_big_endian, typename OutT, bool out_big_endian> static void convertBuffer(void* self, const uint8_t* src_ptr, const size_t src_stride, uint8_t* dst_ptr, const size_t dst_stride, size_t &position, const size_t end_position) {
        for ( ; position<end_position; position++) {
            InT in(src_ptr, in_big_endian);
            OutT out;
            static_cast<SampleConverterImpl*>(self)->convertValue<InT, OutT>(in, out);
            out.writeTo(dst_ptr, out_big_endian);
            src_ptr += src_stride;
            dst_ptr += dst_stride;
        }
    }
};

SampleConverter::SampleConverter(): impl_(make_unique<SampleConverterImpl>()) {
}

SampleConverter::~SampleConverter() {
}

#include "../objs/get_conversion_function.generated.cpp"

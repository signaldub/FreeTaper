#pragma once

#include <boost/asio.hpp>

extern int global_prio_converter;
extern int global_prio_writer;

extern int global_sample_rate;

extern bool global_use_local_time;

extern boost::asio::io_service global_io_service;
